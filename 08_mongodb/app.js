const express = require("express");
const cors = require("cors");
const app = express();
const port = 8080;
const mongoose = require("mongoose");
const { Schema } = mongoose;

mongoose.connect("mongodb+srv://admin:6CnW9NxWknTfA0hq@cluster0.tprtt.mongodb.net/paperDB?retryWrites=true&w=majority");

app.use(cors());

const imageSchema = new Schema({
    path: String,
    width: Number,
    height: Number,
    color: String,
});

const schoolSchema = new Schema({
    name: String,
    image: { type: imageSchema },
});

const studentSchema = new Schema({
    school: { type: Schema.Types.ObjectId, ref: "School" },
    name: { type: String, required: true, lowercase: true, index: true, trim: true },
    surname: String,
    email: { type: String, unique: true },
    age: Number,
    gender: { type: String, enum: ["male", "female", "unknown"] },
    hobby: Array,
    joinAt: {
        index: true,
        type: Date,
        default: () => {
            return Date.now();
        },
    },
});

const School = mongoose.model("School", schoolSchema);
const Student = mongoose.model("Student", studentSchema, "suragch");

app.get("/", (req, res) => {
    const Cat = mongoose.model("Cat", { name: String });
    const kitty = new Cat({ name: "Zildjian" });
    kitty.save().then(() => console.log("meow"));

    res.send("Hello World!");
});

app.get("/schools/create", (req, res) => {
    const { name } = req.query;
    const newSchool = new School({ name: name });
    newSchool.save().then(() => {
        res.json(newSchool);
    });
});

app.get("/schools/list", async (req, res) => {
    // School.find().then((list) => {
    //     res.json(list);
    // });

    // const list = await School.find({ name: "Nest" });
    // const list = await School.find({}, "_id");
    // const list = await School.find({}, null, { sort: { name: -1 } });

    const list = await School.find();
    res.json(list);
});

app.get("/schools/one", async (req, res) => {
    const { id } = req.query;
    // const one = await School.findById(id, "_id");
    const one = await School.findOne({ _id: id });
    res.json(one);
});

app.get("/schools/update", async (req, res) => {
    const { id, name } = req.query;
    const update = await School.updateOne({ _id: id }, { name: name });
    res.json(update);
});

app.get("/schools/delete", async (req, res) => {
    const { id } = req.query;
    const deleted = await School.deleteOne({ _id: id });
    res.json(deleted);
});

app.get("/students/create", (req, res) => {
    const { name, schoolId } = req.query;
    Student.create({ name, school: schoolId }).then((newStudent) => {
        res.json(newStudent);
    });
});

app.get("/students/one", async (req, res) => {
    const { id } = req.query;
    const one = await Student.findOne({ _id: id }).populate("school");
    res.json(one);
});

app.listen(port, () => {
    console.log("backend started");
});
