export default function CategoryEdit({ onClose }) {
    return (
        <div className="modal is-active">
            <div className="modal-background"></div>
            <div className="modal-content" style={{ maxWidth: 400 }}>
                <div className="box">
                    <h2 className="title is-3 has-text-centered">Ангилал нэмэх</h2>

                    <div className="field">
                        <label className="label">Нэр</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Ангиллын нэр..." />
                        </div>
                    </div>

                    <div className="field">
                        <label className="label">Холбооc</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Холбооc..." />
                        </div>
                    </div>

                    <hr />

                    <div className="buttons">
                        <button className="button" onClick={onClose} style={{ flex: 1 }}>
                            Болих
                        </button>
                        <button className="button is-link" style={{ flex: 1 }}>
                            Хадгалах
                        </button>
                    </div>
                </div>
            </div>
            <button className="modal-close is-large" aria-label="close" onClick={onClose}></button>
        </div>
    );
}
