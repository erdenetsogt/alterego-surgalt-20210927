const express = require("express");
const cors = require("cors");
const app = express();

require("./mongoose/connect");

const { ApolloServer } = require("apollo-server-express");

async function startApolloServer(typeDefs, resolvers) {
    const server = new ApolloServer({ typeDefs, resolvers }); //apollo
    const app = express();
    app.use(cors());
    await server.start(); //apollo
    server.applyMiddleware({ app }); //apollo
    app.listen(
        {
            port: 4000,
        },
        () => console.log(`Server ready at http://localhost:4000${server.graphqlPath}`)
    );
}

const typeDefs = require("./graphql/schema");
const resolvers = require("./graphql/resolvers");
startApolloServer(typeDefs, resolvers);
