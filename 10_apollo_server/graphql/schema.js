const { gql } = require("apollo-server-express");

const typeDefs = gql`
    type Query {
        categories(status: String): [Category]
        oneCategory(id: ID): Category
    }

    type Mutation {
        addCategory(name: String!, slug: String): Category
        updateCategory(id: ID!, name: String, slug: String): Boolean
        deleteCategory(id: ID!): Boolean
    }

    type Category {
        _id: ID
        name: String
        slug: String
        status: String
    }
`;

module.exports = typeDefs;
