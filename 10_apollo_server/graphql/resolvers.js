const Category = require("../mongoose/models/Category");

const resolvers = {
    Query: {
        categories: require("./queries/categories"),
        oneCategory: async (parent, args) => {
            const { id } = args;
            const one = await Category.findOne({ _id: id });
            return one;
        },
    },

    Mutation: {
        addCategory: async (parent, args) => {
            const { name, slug } = args;
            const newCategory = await Category.create({ name, slug });
            return newCategory;
        },
        updateCategory: async (parent, args) => {
            const { id, name, slug } = args;
            await Category.updateOne({ _id: id }, { name, slug });
            return true;
        },
        deleteCategory: async (parent, args) => {
            const { id } = args;
            try {
                await Category.deleteOne({ _id: id });
                return true;
            } catch (e) {
                console.error(e);
                return false;
            }
        },
    },
};

module.exports = resolvers;
