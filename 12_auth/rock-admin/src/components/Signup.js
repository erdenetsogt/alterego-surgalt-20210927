import { useState } from "react";

export default function Signup() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const [loading, setLoading] = useState(false);

    function submit() {
        setLoading(true);

        fetch("http://localhost:8080/users/signup", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ username, password }),
        })
            .then((res) => res.json())
            .then((data) => {
                // onComplete();
                // toast.success("Амжилттай бүртгэлээ");
            });
    }

    return (
        <div>
            <div className="container mt-6" style={{ maxWidth: 300 }}>
                <h1 className="title has-text-centered">Бүртгэл</h1>

                <div className="field">
                    <label className="label">Хэрэглэгчийн нэр</label>
                    <div className="control">
                        <input disabled={loading} value={username} onChange={(e) => setUsername(e.target.value)} className="input" type="text" />
                    </div>
                </div>

                <div className="field">
                    <label className="label">Нууц үг</label>
                    <div className="control">
                        <input disabled={loading} value={password} onChange={(e) => setPassword(e.target.value)} className="input" type="password" />
                    </div>
                </div>

                <hr />

                <button className="button is-link is-fullwidth" onClick={submit}>
                    Бүртгүүлэх
                </button>
            </div>
        </div>
    );
}
