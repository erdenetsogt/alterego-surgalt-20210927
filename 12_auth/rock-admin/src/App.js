import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Categories from "./components/Categories";
import Signup from "./components/Signup";
import Login from "./components/Login";
import "bulma/css/bulma.min.css";
import { useEffect, useState } from "react";
import fetcher from "./utils/fetcher";
import Loader from "./components/Loader";

function App() {
    const [me, setMe] = useState();

    useEffect(() => {
        fetcher("me")
            .then((data) => setMe(data))
            .catch(() => setMe(null));
    }, []);

    if (me === undefined) return <Loader />;
    if (me === null) return <Login />;

    return (
        <Router>
            <Switch>
                <Route path="/categories">
                    <Categories />
                </Route>
                <Route path="/signup">
                    <Signup />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
