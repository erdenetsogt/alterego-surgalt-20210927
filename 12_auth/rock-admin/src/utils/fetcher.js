import { toast } from "react-toastify";

export default function fetcher(path) {
    return fetch(`http://localhost:8080/${path}`, {
        headers: {
            token: localStorage.getItem("token"),
        },
    })
        .then((res) => {
            if (res.ok) {
                return res.json();
            } else {
                toast.error(`Алдаа гарлаа (${res.status})`);
                throw res.status;
            }
        })
        .then((data) => data);
}
