import Empty from "./Empty";
import Loader from "./Loader";
import ConfirmDelete from "./ConfirmDelete";
import CategoriesEdit from "./CategoriesEdit";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { gql, useQuery, useMutation } from "@apollo/client";

const GET_CATEGORIES = gql`
    query GetCategories($status: String) {
        categories(status: $status) {
            _id
            name
            slug
            status
        }
    }
`;

const DELETE_CATEGORY = gql`
    mutation DeleteCategory($id: ID!) {
        deleteCategory(id: $id)
    }
`;

export default function CategoriesList({ version }) {
    const { loading, error, data, refetch } = useQuery(GET_CATEGORIES, { variables: { status: "active" } });

    const [deleteCategory, { loading: deleting }] = useMutation(DELETE_CATEGORY, {
        onCompleted() {
            setDeleteId("");
            refetch();
            toast.info("Амжилттай устгалаа");
        },
    });

    const [deleteId, setDeleteId] = useState("");
    const [editing, setEditing] = useState();

    useEffect(() => {
        if (version) {
            refetch();
        }
    }, [version]);

    if (loading) return <Loader />;
    if (error) return <p>Error :(</p>;

    const { categories } = data;

    if (categories.length === 0) return <Empty />;

    return (
        <>
            <table className="table is-fullwidth">
                <thead>
                    <tr>
                        <th>Нэр</th>
                        <th>Холбоос</th>
                        <th style={{ width: 1 }}>Тохиргоо</th>
                    </tr>
                </thead>
                <tbody>
                    {categories.map((item) => (
                        <tr key={item._id}>
                            <td>
                                <strong>
                                    {item.name} {item.status}
                                </strong>
                            </td>
                            <td>
                                <small className="has-text-grey">https://dev.to/t/{item.slug}</small>
                            </td>
                            <td>
                                <div className="buttons" style={{ flexWrap: "nowrap" }}>
                                    <button className="button is-small is-info is-light" onClick={() => setEditing(item)}>
                                        Засах
                                    </button>
                                    <button disabled={deleting} className="button is-small is-danger is-light" onClick={() => setDeleteId(item._id)}>
                                        Устгах
                                    </button>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {deleteId && <ConfirmDelete onConfirm={() => deleteCategory({ variables: { id: deleteId } })} onClose={() => setDeleteId("")} />}
            {editing && (
                <CategoriesEdit
                    category={editing}
                    onClose={() => setEditing(null)}
                    onComplete={() => {
                        setEditing(null);
                        refetch();
                    }}
                />
            )}
        </>
    );
}
