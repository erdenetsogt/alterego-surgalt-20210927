export default function Loader() {
    return (
        <div className="has-text-centered my-6">
            <span className="button is-loading is-large is-warning" style={{ background: "transparent" }}></span>
        </div>
    );
}
