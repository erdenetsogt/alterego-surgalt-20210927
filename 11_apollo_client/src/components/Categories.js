import Header from "./Header";
import CategoriesNew from "./CategoriesNew";
import CategoriesList from "./CategoriesList";
import { useState } from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Categories() {
    const [creating, setCreating] = useState(false);
    const [version, setVersion] = useState(0);

    function handleComplete() {
        setCreating(false);
        setVersion(version + 1);
    }

    return (
        <div>
            <ToastContainer position="bottom-right" />
            <Header />

            <div className="container mt-6" style={{ maxWidth: 700 }}>
                <div className="is-flex is-justify-content-space-between">
                    <h1 className="title is-2">Ангилал</h1>

                    <button className="button is-success" onClick={() => setCreating(true)}>
                        Шинэ
                    </button>
                </div>

                <CategoriesList version={version} />

                {creating && <CategoriesNew onClose={() => setCreating(false)} onComplete={handleComplete} />}
            </div>
        </div>
    );
}
