import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Categories from "./components/Categories";
import "bulma/css/bulma.min.css";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";

const client = new ApolloClient({
    uri: "http://localhost:4000/graphql",
    cache: new InMemoryCache(),
});

function App() {
    return (
        <ApolloProvider client={client}>
            <Router>
                <Switch>
                    <Route path="/categories">
                        <Categories />
                    </Route>
                </Switch>
            </Router>
        </ApolloProvider>
    );
}

export default App;
