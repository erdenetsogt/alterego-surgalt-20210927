const express = require("express");
const cors = require("cors");
const app = express();
const port = 8080;

app.use(cors());
app.use(express.static("public"));
app.use((req, res, next) => {
    console.log("middleware");
    const loggedin = true;
    if (loggedin) {
        next();
    } else {
        res.send("Forbidden");
    }
});

app.get(
    "/",
    (req, res, next) => {
        console.log("root middleware");
        next();
    },
    (req, res) => {
        res.send("Hello World!");
    }
);

app.post("/api/categories/post", (req, res) => {
    res.send("Blog");
});

app.get("/api/articles", (req, res) => {
    const list = [
        { id: 1, name: "Д.Мөнхтуул: Вьетнам засварын газруудаас НӨАТ-ын баримтаа заавал авч байгаарай" },
        { id: 2, name: "Д.Мөнхтуул: Вьетнам засварын газруудаас НӨАТ-ын баримтаа заавал авч байгаарай" },
    ];
    res.json(list);
});

app.get("/api/categories", (req, res) => {
    const list = [
        { id: 1, name: "Улс төр", slug: "politics" },
        { id: 2, name: "Эдийн засаг", slug: "economy" },
        { id: 3, name: "Нийгэм", slug: "society" },
    ];
    res.json(list);
});

app.get("/n/:articleId/comment/:commentId", (req, res) => {
    const { articleId } = req.params;
    console.log(articleId);
    console.log(req.query);
    console.log(req.params);

    res.send("Article");
});

const categoriesRouter = require("./router/categories");

app.use("/categories", categoriesRouter);

app.listen(port, () => {
    console.log("backend started");
});
