import Header from "./components/Header";
import Footer from "./components/Footer";
import { useState, useEffect } from "react";

function App() {
    const [name, setName] = useState("Erdene");
    const [age, setAge] = useState(30);
    const [footerVisible, setFooterVisible] = useState(true);

    const green = "#00FF00";

    let text = "Hello";

    function changeText() {
        setName("Bold");
    }

    useEffect(() => {
        console.log("Бүх өөрчлөлт");
    });

    useEffect(() => {
        console.log("Анх зурах үед нэг удаа");
    }, []);

    useEffect(() => {
        console.log("name өөрчлөгдөх болгонд");
    }, [name]);

    useEffect(() => {
        console.log("name болон age өөрчлөгдөх болгонд");
    }, [name, age]);

    return (
        <>
            <div className="App">
                <br />
                {name}
                <Header name="Bold" age={28} />

                <h1>{text}</h1>
                <button onClick={changeText}>change</button>

                <p style={{ backgroundColor: "grey", color: green }}>paragraph</p>

                <hr />

                <Main />

                <button onClick={() => setFooterVisible(!footerVisible)}>Toggle footer</button>

                {footerVisible && <Footer name="Sarnai" age={18} />}
            </div>
            <div></div>
        </>
    );
}

export default App;

function Main() {
    return (
        <main>
            <h1>Main</h1>
        </main>
    );
}
