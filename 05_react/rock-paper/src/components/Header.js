import { AwesomeButton } from "react-awesome-button";
import "react-awesome-button/dist/styles.css";

export default function Header(props) {
    return (
        <header>
            <AwesomeButton type="primary">Button</AwesomeButton>
            <h1>
                Header 1, {props.name} {props.age}
            </h1>
        </header>
    );
}
