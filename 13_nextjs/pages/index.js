import Link from "next/link";
import Image from "next/image";

export default function Home() {
    return (
        <>
            <Comp />
            <p>Hello</p>
            <Link href="/about">
                <a>About</a>
            </Link>
            <style jsx>{`
                p {
                    color: green;
                }
                a {
                    color: pink;
                }
            `}</style>
        </>
    );
}

function Comp() {
    return (
        <>
            <img src="/coding.jpg" style={{ width: 300 }} alt />
            <Image src="/coding.jpg" width="38" height="25" alt />
        </>
    );
}
