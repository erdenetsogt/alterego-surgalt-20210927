import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Categories from "./components/Categories";
import "bulma/css/bulma.min.css";

function App() {
    return (
        <Router>
            <Switch>
                <Route path="/categories">
                    <Categories />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
