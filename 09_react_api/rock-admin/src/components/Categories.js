import Header from "./Header";
import CategoriesNew from "./CategoriesNew";
import CategoriesList from "./CategoriesList";
import { useEffect, useState } from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Categories() {
    const [creating, setCreating] = useState(false);
    const [list, setList] = useState();

    useEffect(() => {
        loadList();
    }, []);

    function loadList() {
        fetch("http://localhost:8080/categories")
            .then((res) => res.json())
            .then((data) => setList(data));
    }

    function handleComplete() {
        setCreating(false);
        loadList();
    }

    return (
        <div>
            <ToastContainer position="bottom-right" />
            <Header />

            <div className="container mt-6" style={{ maxWidth: 700 }}>
                <div className="is-flex is-justify-content-space-between">
                    <h1 className="title is-2">Ангилал</h1>

                    <button className="button is-success" onClick={() => setCreating(true)}>
                        Шинэ
                    </button>
                </div>

                <CategoriesList list={list} onChange={() => loadList()} />

                {creating && <CategoriesNew onClose={() => setCreating(false)} onComplete={handleComplete} />}
            </div>
        </div>
    );
}
