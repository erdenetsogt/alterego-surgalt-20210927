import Empty from "./Empty";
import Loader from "./Loader";
import ConfirmDelete from "./ConfirmDelete";
import CategoriesEdit from "./CategoriesEdit";
import { useState } from "react";
import { toast } from "react-toastify";

export default function CategoriesList({ list, onChange }) {
    const [deleteId, setDeleteId] = useState("");
    const [editing, setEditing] = useState();

    if (!list) return <Loader />;
    if (list.length === 0) return <Empty />;

    function handleDelete() {
        fetch(`http://localhost:8080/categories/${deleteId}`, {
            method: "DELETE",
        })
            .then((res) => res.json())
            .then((data) => {
                setDeleteId("");
                onChange();
                toast.info("Амжилттай устгалаа");
            });
    }

    return (
        <>
            <table className="table is-fullwidth">
                <thead>
                    <tr>
                        <th>Нэр</th>
                        <th>Холбоос</th>
                        <th style={{ width: 1 }}>Тохиргоо</th>
                    </tr>
                </thead>
                <tbody>
                    {list.map((item) => (
                        <tr key={item._id}>
                            <td>
                                <strong>{item.name}</strong>
                            </td>
                            <td>
                                <small className="has-text-grey">https://dev.to/t/{item.slug}</small>
                            </td>
                            <td>
                                <div className="buttons" style={{ flexWrap: "nowrap" }}>
                                    <button className="button is-small is-info is-light" onClick={() => setEditing(item)}>
                                        Засах
                                    </button>
                                    <button className="button is-small is-danger is-light" onClick={() => setDeleteId(item._id)}>
                                        Устгах
                                    </button>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {deleteId && <ConfirmDelete onConfirm={handleDelete} onClose={() => setDeleteId("")} />}
            {editing && (
                <CategoriesEdit
                    category={editing}
                    onClose={() => setEditing(null)}
                    onComplete={() => {
                        setEditing(null);
                        onChange();
                    }}
                />
            )}
        </>
    );
}
